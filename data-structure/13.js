/*
A foods object has been created with three entries. 
Using the syntax of your choice, add three more entries to it: bananas with a value of 13, grapes with a value of 35, and strawberries with a value of 27.

- foods should be an object.
- The foods object should have a key "bananas" with a value of 13.
- The foods object should have a key "grapes" with a value of 35.
- The foods object should have a key "strawberries" with a value of 27.
- The key-value pairs should be set using dot or bracket notation.
*/

let foods = {
    apples: 25,
    oranges: 32,
    plums: 28
  };
  
  // Only change code below this line
  const fruits = ['bananas', 'grapes', 'strawberries'];
  foods[fruits[0]] = 13;
  foods[fruits[1]] = 35;
  foods[fruits[2]] = 27;
  // Only change code above this line
  
  console.log(foods);

  /*
  { apples: 25, oranges: 32, plums: 28, bananas: 13, grapes: 35, strawberries: 27 }
  */