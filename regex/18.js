/*
Use the shorthand character class \w to count the number of alphanumeric characters in various quotes and strings.

- Your regex should use the global flag.
- Your regex should use the shorthand character \w to match all characters which are alphanumeric.
- Your regex should find 31 alphanumeric characters in "The five boxing wizards jump quickly."
- Your regex should find 32 alphanumeric characters in "Pack my box with five dozen liquor jugs."
- Your regex should find 30 alphanumeric characters in "How vexingly quick daft zebras jump!"
- Your regex should find 36 alphanumeric characters in "123 456 7890 ABC def GHI jkl MNO pqr STU vwx YZ."
*/

let quoteSample = "The five boxing wizards jump quickly.";
let alphabetRegexV2 = /\w/g; // Change this line
let result = quoteSample.match(alphabetRegexV2).length;
