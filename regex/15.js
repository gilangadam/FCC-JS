/*
Write a greedy regex that finds one or more criminals within a group of other people. 
A criminal is represented by the capital letter C.

- Your regex should match one criminal (C) in "C"
- Your regex should match two criminals (CC) in "CC"
- Your regex should match three criminals (CCC) in "P1P5P4CCCP2P6P3"
- Your regex should match five criminals (CCCCC) in "P6P2P7P4P5CCCCCP3P1"
- Your regex should not match any criminals in ""
- Your regex should not match any criminals in "P1P2P3"
- Your regex should match fifty criminals (CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC) in "P2P1P5P4CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCP3".
*/


let reCriminals = /C+/g; // Change this line