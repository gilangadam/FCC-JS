/*
Change the regex ohRegex to match the entire phrase "Oh no" only when it has 3 to 6 letter h's.

- Your regex should use curly brackets.
- Your regex should not match "Ohh no"
- Your regex should match "Ohhh no"
- Your regex should match "Ohhhh no"
- Your regex should match "Ohhhhh no"
- Your regex should match "Ohhhhhh no"
- Your regex should not match "Ohhhhhhh no"
*/

let ohStr = "Ohhh no";
let ohRegex = /oh{3,6}\sno/i; // Change this line
let result = ohRegex.test(ohStr);
