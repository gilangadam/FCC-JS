/*
There are two kinds of lookaheads: positive lookahead and negative lookahead.
A positive lookahead will look to make sure the element in the search pattern is there, but won't actually match it. 
A positive lookahead is used as (?=...) where the ... is the required part that is not matched.
On the other hand, a negative lookahead will look to make sure the element in the search pattern is not there. 
A negative lookahead is used as (?!...) where the ... is the pattern that you do not want to be there. 
The rest of the pattern is returned if the negative lookahead part is not present.

A more practical use of lookaheads is to check two or more patterns in one string. 
Here is a (naively) simple password checker that looks for between 3 and 6 characters and at least one number:
let password = "abc123";
let checkPass = /(?=\w{3,6})(?=\D*\d)/;
checkPass.test(password); // Returns true


Use lookaheads in the pwRegex to match passwords that are greater than 5 characters long, do not begin with numbers, and have two consecutive digits.
- Your regex should use two positive lookaheads.
- Your regex should not match "astronaut"
- Your regex should not match "banan1"
- Your regex should match "bana12"
- Your regex should match "abc123"
- Your regex should not match "1234"
- Your regex should not match "8pass99"
- Your regex should not match "12abcde"
- Your regex should match "astr1on11aut"
*/



let sampleWord = "astronaut";
let pwRegex = /^\D(?=\w{5,})(?=\w*\d{2})/; // Change this line
let result = pwRegex.test(sampleWord);
