/*
For this challenge, chewieQuote has been initialized as "Aaaaaaaaaaaaaaaarrrgh!" behind the scenes. Create a regex chewieRegex that uses the * character to match an uppercase "A" character immediately followed by zero or more lowercase "a" characters in chewieQuote. Your regex does not need flags or character classes, and it should not match any of the other quotes.

- Your regex chewieRegex should use the * character to match zero or more a characters.
- Your regex should match "A" in chewieQuote.
- Your regex should match "Aaaaaaaaaaaaaaaa" in chewieQuote.
- Your regex chewieRegex should match 16 characters in chewieQuote.
- Your regex should not match any characters in "He made a fair move. Screaming about it can't help you."
- Your regex should not match any characters in "Let him have it. It's not wise to upset a Wookiee."
*/

// Only change code below this line
let chewieRegex = /Aa*/; // Change this line
// Only change code above this line

let result = chewieQuote.match(chewieRegex);